<?php

declare(strict_types=1);

namespace Pay\Domain\User;

use Assert\Assertion;

final class Password
{
    private string $password;

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function __toString(): string
    {
        return $this->password;
    }

    public function hasPassword(): self
    {
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);

        return $this;
    }
}
