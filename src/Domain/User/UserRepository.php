<?php

declare(strict_types=1);

namespace Pay\Domain\User;

interface UserRepository
{
    public function mustFindUserById(Id $userId): User;

    public function mustFindByEmail(Email $email): ?User;

    public function userWithEmailExists(Email $email): bool;

    public function insert(User $user): void;

    public function update(User $user): User;

    public function getAll(): iterable;
}
