<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

final class UserAlreadyExistsException extends \Exception
{
    public static function withEmail(string $email): self
    {
        return new self(sprintf('User with email %s already exists.', $email));
    }
}
