<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

final class InvalidUserRoleException extends \Exception
{
    public static function forRole(string $role): self
    {
        return new  self(sprintf('User role %s is not valid.', $role));
    }
}
