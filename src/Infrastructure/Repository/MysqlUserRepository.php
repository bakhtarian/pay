<?php

declare(strict_types=1);

namespace Pay\Infrastructure\Repository;

use Pay\Domain\User\Email;
use Pay\Domain\User\Id;
use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;
use Pay\Exception\UserNotFoundException;
use function DI\string;

final class MysqlUserRepository implements UserRepository
{
    private DataBase $database;

    public function __construct(DataBase $database)
    {
        $this->database = $database;
    }

    public function mustFindUserById(Id $userId): User
    {
        $sql = <<<SQL
SELECT * FROM users
WHERE id = ?
SQL;
        $id = (string) $userId;
        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param('s', $id);
        $statement->execute();

        $result = $statement->get_result()->fetch_assoc();

        if ($result === null) {
            throw UserNotFoundException::withId($userId);
        }

        return User::fromArray($result);
    }

    public function mustFindByEmail(Email $email): ?User
    {
        $sql = <<<SQL
SELECT * FROM users
WHERE email = ?
SQL;

        $userEmail = (string) $email;
        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param('s', $userEmail);
        $statement->execute();

        $result = $statement->get_result()->fetch_assoc();

        if ($result !== null) {
            return User::fromArray($result);
        }

        return null;
    }

    public function insert(User $user): void
    {
        $sql = <<<SQL
INSERT INTO users (id, email, password, role, active, created_at)
VALUES (?, ?, ?, ?, ?, ?)
SQL;

        $mysql = $this->database->getMysql();

        $email = $mysql->real_escape_string((string) $user->getEmail());
        $password = $mysql->real_escape_string((string) $user->getPassword());

        $id = (string) $user->getId();
        $role = $user->getRole()->getAccessLevel();
        $active = (string) $user->isActive();
        $createdAt = $user->getCreatedAt()->format(DATE_ATOM);

        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param(
            "ssssss",
            $id,
            $email,
            $password,
            $role,
            $active,
            $createdAt
        );

        $statement->execute();
    }

    public function update(User $user): User
    {
        $sql = <<<SQL
UPDATE users SET email = ?, password = ?, role = ?, active =?, modified_at = ?
WHERE id = ?
SQL;
        $email = $this->database->getMysql()->real_escape_string((string) $user->getEmail());
        $password = $this->database->getMysql()->real_escape_string((string) $user->getPassword());

        $id = (string) $user->getId();
        $role = $user->getRole()->getAccessLevel();
        $active = (string) $user->isActive();
        $modifiedAt = $user->getModifiedAt()->format(DATE_ATOM);

        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param(
            "ssssss",
            $email,
            $password,
            $role,
            $active,
            $modifiedAt,
            $id
        );

        $statement->execute();

        return $this->mustFindUserById($user->getId());
    }

    public function userWithEmailExists(Email $email): bool
    {
        $sql = <<<SQL
SELECT id from users
WHERE email = ?
SQL;

        $userEmail = (string) $email;
        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param('s', $userEmail);
        $statement->execute();

        $result = $statement->fetch();

        if ($result === null) {
            return  false;
        }

        return true;
    }

    public function getAll(): iterable
    {
        $sql = <<<SQL
SELECT * FROM users
WHERE active = ?
SQL;

        $active = (string) true;
        $statement = $this->database->getMysql()->prepare($sql);
        $statement->bind_param('s', $active);
        $statement->execute();

        $results = $statement->get_result()->fetch_all(MYSQLI_ASSOC);

        if (empty($results)) {
            return null;
        }

        foreach ($results as $user) {
            yield User::fromArray($user);
        }
    }
}
