<?php

declare(strict_types=1);

namespace Pay\Application;

use Pay\Domain\Exception\NotAllowedToChangeUserException;
use Pay\Domain\User\Email;
use Pay\Domain\User\Password;
use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;
use Psr\Http\Message\ServerRequestInterface;

final class EditUser
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function upDateUser(User $userToUpdate, User $userUpdating, ServerRequestInterface $request): User
    {
        $input = $request->getParsedBody();

        try {
            $userToUpdate->mutabilityAllowed($userUpdating);
        } catch (NotAllowedToChangeUserException $exception) {
            throw $exception;
        }

        $email = new Email($input['email']);
        $password = new Password($input['password']);
        $user = $_SESSION['user'];

        $userToUpdate->changeEmail((string)$email, $user);
        $userToUpdate->changePassword((string) $password, $user);
        if ($input['role'] !== null) {
            $userToUpdate->changeRole($input['role'], $user);
        }

        $this->repository->update($userToUpdate);

        return $userToUpdate;
    }
}
