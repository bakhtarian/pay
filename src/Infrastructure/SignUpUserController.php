<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Application\SignUpUser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

final class SignUpUserController
{
    use UserRedirectBasedOnLogin;

    private Environment $twig;
    private SignUpUser $user;
    private Session $session;

    public function __construct(
        Environment $twig,
        SignUpUser $user,
        Session $session
    ) {
        $this->twig = $twig;
        $this->user = $user;
        $this->session = $session;
    }

    public function index(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if ($this->isLoggedIn() instanceof RedirectResponse) {
            return $this->isLoggedIn();
        }

        if ($request->getMethod() === 'POST') {
            $input = $request->getParsedBody();

            if (isset($input['csrf']) === false) {

                $this->session->generateCsrfToken();

                $context = [
                    'error' => 'Invalid form',
                    'csrf' => $this->session->getCsrfToken()
                ];

                $response->getBody()->write($this->twig->render('sign_up.html.twig',$context));

                return $response;
            }

            if ($_SESSION['csrf'] !== $_POST['csrf']) {

                $this->session->generateCsrfToken();

                $context = [
                    'error' => 'Invalid form',
                    'csrf' => $this->session->getCsrfToken()
                ];

                $response->getBody()->write($this->twig->render('sign_up.html.twig',$context));

                return $response;
            }

            $this->user->signUp($input['email'], $input['password']);

            $response = new RedirectResponse('/');
            $response->getBody()->write($this->twig->render('login.html.twig', ['succes' => 'You can now login']));

            return $response;
        }

        $this->session->resetSessionAfterXMinutes(5);
        $this->session->generateCsrfToken();

        $context = [
            'csrf' => $this->session->getCsrfToken()
        ];

        $response->getBody()->write($this->twig->render('sign_up.html.twig', $context));

        return $response;
    }
}
