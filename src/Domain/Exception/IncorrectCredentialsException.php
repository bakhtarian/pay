<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

final class IncorrectCredentialsException extends \Exception
{
    public static function forLogin(): self
    {
        return new self('Incorrect credentials.');
    }
}
