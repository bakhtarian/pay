<?php

declare(strict_types=1);

namespace Pay\Infrastructure\Repository;

final class DataBase
{
    private \mysqli $mysql;

    public function __construct(
        $host,
        $user,
        $password,
        $database
    )
    {
        $this->mysql = new \mysqli(
            $host,
            $user,
            $password,
            $database
        );
    }

    public function getMysql(): \mysqli
    {
        return $this->mysql;
    }
}
