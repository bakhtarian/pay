<?php

use DI\ContainerBuilder;
use League\Route\RouteCollection;
use Pay\Router;

require dirname(__DIR__).'/vendor/autoload.php';

if (!session_start()) {
    session_start();
}

$builder = new ContainerBuilder();
$builder->addDefinitions(dirname(__DIR__).'/config/config.php');
$container = $builder->build();

$route = new RouteCollection($container);
Router::routes($route);

$response = $route->dispatch($container->get('request'), $container->get('response'));
$container->get('emitter')->emit($response);

if (isset($_SESSION['time_out']) === false) {
    $_SESSION['time_out'] = time();
}
