<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

use Pay\Domain\User\Id;

final class UserNotFoundException extends \Exception
{
    public static function withId(Id $id)
    {
        return new self(sprintf('User with id %s not fount', (string) $id));
    }
}
