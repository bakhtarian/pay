<?php

declare(strict_types=1);

namespace Pay\Application;

use Pay\Domain\Exception\CanNotTerminateYourselfException;
use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;

final class TerminateUser
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function terminate(User $userToTerminate, User $userTerminating): User
    {
        if ($userToTerminate === $userTerminating) {
            throw CanNotTerminateYourselfException::withId($userTerminating->getId());
        }

        $userToTerminate->terminate($userTerminating);
        $this->repository->update($userToTerminate);

        return $userToTerminate;
    }
}
