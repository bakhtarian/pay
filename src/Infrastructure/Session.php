<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;

final class Session
{
    private ?string $csrfToken;

    public function __construct()
    {
        $this->csrfToken = null;

        if ($_SESSION['csrf']) {
            $this->csrfToken = $_SESSION['csrf'];
        }
    }
    public function resetSessionAfterXMinutes(int $minutes): void
    {
        $timeSinceLastAction = time() - $_SESSION['time_out'];

        if ($timeSinceLastAction >= ($minutes * 60)) {
            if (isset($_SESSION['csrf'])) {
                unset($_SESSION['csrf']);
            }
        }
    }

    public function generateCsrfToken(): void
    {
        if (isset($_SESSION['csrf'])) {
            unset($_SESSION['csrf']);
        }

        $this->csrfToken = bin2hex(random_bytes(32));
        $_SESSION['csrf'] = $this->csrfToken;
    }

    public function getCsrfToken(): string
    {
        return $_SESSION['csrf'];
    }

    public function validCsrf(
        ServerRequestInterface $request,
        ResponseInterface $response,
        Environment $twig
    ): ?ResponseInterface {
        $input = $request->getParsedBody();

        if (isset($input['csrf']) === false) {

            $this->generateCsrfToken();

            $context = [
                'error' => 'Invalid form',
                'csrf' => $this->getCsrfToken()
            ];

            $response->getBody()->write($twig->render('sign_up.html.twig', $context));

            return $response;
        }

        if ($_SESSION['csrf'] !== $_POST['csrf']) {

            $this->generateCsrfToken();

            $context = [
                'error' => 'Invalid form',
                'csrf' => $this->getCsrfToken()
            ];

            $response->getBody()->write($twig->render('sign_up.html.twig', $context));

            return $response;
        }

        return null;
    }
}
