<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Application\TerminateUser;
use Pay\Domain\Exception\CanNotTerminateYourselfException;
use Pay\Domain\Exception\NotAllowedToChangeUserException;
use Pay\Domain\Exception\UserNotFoundException;
use Pay\Domain\User\Id;
use Pay\Domain\User\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

final class TerminateUserController
{
    use UserRedirectBasedOnLogin;

    private Session $session;
    private Environment $twig;
    private TerminateUser $terminateUser;
    private UserRepository $repository;

    public function __construct(
        Session $session,
        Environment $twig,
        TerminateUser $terminateUser,
        UserRepository $repository
    ) {
        $this->session = $session;
        $this->twig = $twig;
        $this->terminateUser = $terminateUser;
        $this->repository = $repository;
    }

    public function index(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments
    ): ResponseInterface
    {
        if ($this->mayViewPage() instanceof RedirectResponse) {
            return $this->mayViewPage();
        }

        try {
            $user = $this->repository->mustFindUserById(new Id($arguments['userId']));
        } catch (UserNotFoundException $exception) {
            return new RedirectResponse('/overview');
        }

        try {
            $this->terminateUser->terminate($user, $_SESSION['user']);
        } catch (NotAllowedToChangeUserException | CanNotTerminateYourselfException $exception) {
            $this->session->generateCsrfToken();

            $context = [
                'userToTerminate' => $user,
                'user' => $_SESSION['user'],
                'users' => $this->repository->getAll(),
                'csrf' => $this->session->getCsrfToken(),
                'error' => $exception->getMessage(),
            ];

            $response->getBody()->write($this->twig->render('overview.html.twig', $context));

            return $response;
        }

        return new RedirectResponse('/overview');
    }
}
