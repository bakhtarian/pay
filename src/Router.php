<?php

declare(strict_types=1);

namespace Pay;

use League\Route\RouteCollectionInterface;
use Pay\Infrastructure\EditUserController;
use Pay\Infrastructure\HomePageController;
use Pay\Infrastructure\LogOutController;
use Pay\Infrastructure\OverviewController;
use Pay\Infrastructure\SignUpUserController;
use Pay\Infrastructure\TerminateUserController;

final class Router
{
    public static function routes(RouteCollectionInterface $route): RouteCollectionInterface
    {
        $route->get('/', HomePageController::class . '::index');
        $route->post('/', HomePageController::class . '::index');
        $route->get('/login', HomePageController::class . '::index');
        $route->post('/login', HomePageController::class . '::index');

        $route->get('/sign-up', SignUpUserController::class . '::index');
        $route->post('/sign-up', SignUpUserController::class . '::index');

        $route->get('/overview', OverviewController::class. '::index');

        $route->get('/logout', LogOutController::class. '::index');

        $route->get('/edit/{userId}', EditUserController::class . '::index');
        $route->post('/edit/{userId}', EditUserController::class . '::index');

        $route->get('/terminate/{userId}', TerminateUserController::class . '::index');

        return $route;
    }
}
