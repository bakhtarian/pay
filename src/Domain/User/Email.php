<?php

declare(strict_types=1);

namespace Pay\Domain\User;

use Assert\Assertion;

final class Email
{
    private string $email;

    public function __construct(string $email)
    {
        Assertion::email($email);
        $this->email = $email;
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
