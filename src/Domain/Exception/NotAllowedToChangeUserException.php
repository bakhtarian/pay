<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

use Pay\Domain\User\Id;

final class NotAllowedToChangeUserException extends \Exception
{
    public static function forUser(Id $forUser, Id $byUser): self
    {
        return new self(sprintf('User with id %s can not be edited by user with id %s', $forUser, $byUser));
    }
}
