<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Application\LoginUser;
use Pay\Domain\User\Email;
use Pay\Domain\User\Password;
use Pay\Domain\Exception\IncorrectCredentialsException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

final class HomePageController
{
    use UserRedirectBasedOnLogin;

    private Environment $twig;
    private Session $session;
    private LoginUser $loginUser;

    public function __construct(
        Environment $twig,
        Session $session,
        LoginUser $loginUser
    ) {
        $this->twig = $twig;
        $this->session = $session;
        $this->loginUser = $loginUser;
    }

    public function index(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if ($this->isLoggedIn() instanceof RedirectResponse) {
            return $this->isLoggedIn();
        }

        if ($request->getMethod() === 'POST') {
            $input = $request->getParsedBody();

            if (isset($input['email']) === false && isset($input['password']) === false) {
                $response->getBody()->write($this->twig->render('login.html.twig', [
                    'error' => 'No email and password provided',
                ]));

                return $response;
            }

            if (isset ($input['email']) === false) {
                $response->getBody()->write($this->twig->render('login.html.twig', [
                    'error' => 'No email provided',
                ]));

                return $response;
            }

            if (isset($input['password']) === false) {
                $response->getBody()->write($this->twig->render('login.html.twig', [
                    'error' => 'No password provided',
                ]));

                return $response;
            }

            $email = new Email($input['email']);
            $password = new Password($input['password']);

            try {
                $user = $this->loginUser->login($email, $password);
            } catch (IncorrectCredentialsException $exception) {
                $response->getBody()->write($this->twig->render('login.html.twig', [
                    'error' => $exception->getMessage(),
                ]));

                return $response;
            }

            return new RedirectResponse('/overview');
        }

        $response->getBody()->write($this->twig->render('login.html.twig'));

        return $response;
    }
}
