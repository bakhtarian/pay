<?php

declare(strict_types=1);

namespace Pay\Domain\Exception;

use Pay\Domain\User\Id;

final class CanNotTerminateYourselfException extends \Exception
{
    public static function withId(Id $userId): self
    {
        return new self(sprintf('You can not delete yourself. Tried to delete user with id %s', (string) $userId));
    }
}
