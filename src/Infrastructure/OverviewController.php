<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

final class OverviewController
{
    use UserRedirectBasedOnLogin;

    private Environment $twig;
    private UserRepository $repository;

    public function __construct(
        Environment $twig,
        UserRepository $repository
    ) {
        $this->twig = $twig;
        $this->repository = $repository;
    }

    public function index(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if ($this->mayViewPage() instanceof RedirectResponse) {
            return $this->mayViewPage();
        }

        $users = $this->repository->getAll();

        $response->getBody()->write($this->twig->render('overview.html.twig', [
            'user' => $_SESSION['user'],
            'users' => $users,
        ]));

        return $response;
    }
}
