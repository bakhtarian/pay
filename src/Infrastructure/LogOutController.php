<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\RedirectResponse;

final class LogOutController
{
    public function index(): ResponseInterface {
        unset($_SESSION['user']);

        return new RedirectResponse('/login');
    }
}
