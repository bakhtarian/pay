<?php

declare(strict_types=1);

namespace Pay\Application;

use Pay\Domain\Exception\UserAlreadyExistsException;
use Pay\Domain\User\Email;
use Pay\Domain\User\Password;
use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;

final class SignUpUser
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function signUp(string $email, string $password): User
    {
        $email = new Email($email);
        $password = new Password($password);

        if ($this->repository->userWithEmailExists($email)) {
            throw UserAlreadyExistsException::withEmail((string) $email);
        }

        $user = User::create($email, $password);

        $this->repository->insert($user);

        return $user;
    }
}
