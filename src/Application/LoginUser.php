<?php

declare(strict_types=1);

namespace Pay\Application;

use Pay\Domain\User\Email;
use Pay\Domain\User\Password;
use Pay\Domain\User\User;
use Pay\Domain\User\UserRepository;
use Pay\Domain\Exception\IncorrectCredentialsException;

final class LoginUser
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function login(Email $email, Password $password): User
    {
        $user = $this->repository->mustFindByEmail($email);

        if ($user === null ) {
            throw IncorrectCredentialsException::forLogin();
        }

        if (password_verify((string) $password, (string) $user->getPassword()) === false) {
            throw IncorrectCredentialsException::forLogin();
        }

        $_SESSION['user'] = $user;

        return $user;
    }
}
