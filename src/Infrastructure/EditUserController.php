<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Application\EditUser;
use Pay\Domain\Exception\NotAllowedToChangeUserException;
use Pay\Domain\User\Id;
use Pay\Domain\User\Role;
use Pay\Domain\User\UserRepository;
use Pay\Domain\Exception\UserNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

final class EditUserController
{
    use UserRedirectBasedOnLogin;

    private UserRepository $repository;
    private Environment $twig;
    /**
     * @var Session
     */
    private Session $session;
    /**
     * @var EditUser
     */
    private EditUser $editUser;

    public function __construct(
        UserRepository $repository,
        Environment $twig,
        Session $session,
        EditUser $editUser
    ) {
        $this->repository = $repository;
        $this->twig = $twig;
        $this->session = $session;
        $this->editUser = $editUser;
    }

    public function index(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $arguments
    ): ResponseInterface
    {
        if ($this->mayViewPage() instanceof RedirectResponse) {
            return $this->mayViewPage();
        }

        try {
            $user = $this->repository->mustFindUserById(new Id($arguments['userId']));
        } catch (UserNotFoundException $exception) {
            return new RedirectResponse('/overview');
        }

        $roles = [
            Role::BASE_USER_NAME,
            Role::ADMIN_USER_NAME,
            Role::SUPER_USER_NAME
        ];

        if ($request->getMethod() === 'POST') {
            $this->session->validCsrf($request, $response, $this->twig);
            try {
                $this->editUser->upDateUser($user, $_SESSION['user'], $request);
            } catch (NotAllowedToChangeUserException $exception) {
                $this->session->generateCsrfToken();

                $context = [
                    'userToEdit' => $user,
                    'user' => $_SESSION['user'],
                    'csrf' => $this->session->getCsrfToken(),
                    'roles' => $roles,
                    'error' => $exception->getMessage(),
                ];

                $response->getBody()->write($this->twig->render('edit_user.html.twig', $context));

                return $response;
            }

            return new RedirectResponse('/overview');
        }

        $this->session->generateCsrfToken();

        $context = [
            'userToEdit' => $user,
            'user' => $_SESSION['user'],
            'csrf' => $this->session->getCsrfToken(),
            'roles' => $roles,
        ];

        $response->getBody()->write($this->twig->render('edit_user.html.twig', $context));

        return $response;
    }
}
