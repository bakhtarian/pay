# Pay opdracht

Hieronder volgt een korte uitleg over hoe het project gestart en bekeken kan worden in de browser. Vanuit wordt gegsan dat alle poorten die vermeld zijn in de `docker-compose` beschikbaar zijn.

## Hosts

Om het project te kunnen bezoeken moet het volgende worden toegevoegd aan de `/etc/hosts` :

````
127.0.0.1 pay.test-project.local
````

## Het project

Om het project op te starten, moet er vanuit de root de volgende command worden gedraaid: `make up`, vervolgens `make install`.

### Gebruikers

Er kan in worden gelogd met de volgende gebruikers:

| mail         | wachtwoord | rol       |
|--------------|------------|-----------|
| base@pay.nl  | base       | minaal    |
| admin@pay.nl | admin      | midden    |
| super@pay.nl | super      | kan alles |

Een account aanmaken kan via [hier](pay.test-project.local/sign-up)
