up:
	docker-compose up -d

down:
	docker-compose down

install:
	docker-compose run --rm php composer install
