<?php

declare(strict_types=1);

namespace Pay\Domain\User;

use Pay\Domain\Exception\InvalidUserRoleException;

final class Role
{
    private const TERMINATED_USER_ACCESS = 0;
    private const BASE_USER_ACCESS = 1;
    private const ADMIN_USER_ACCESS = 2;
    private const SUPER_USER_ACCESS = 3;

    public const TERMINATED_USER_NAME = 'removed_user';
    public const BASE_USER_NAME = 'base_user';
    public const ADMIN_USER_NAME = 'admin_user';
    public const SUPER_USER_NAME = 'super_user';

    private const USER_ROLES = [
        self::TERMINATED_USER_NAME => self::TERMINATED_USER_ACCESS,
        self::BASE_USER_NAME => self::BASE_USER_ACCESS,
        self::ADMIN_USER_NAME => self::ADMIN_USER_ACCESS,
        self::SUPER_USER_NAME => self::SUPER_USER_ACCESS
    ];

    private string $role;

    public function __construct(string $role)
    {
        if (!array_key_exists($role, self::USER_ROLES)) {
            throw InvalidUserRoleException::forRole($role);
        }

        $this->role = $role;
    }

    public function getAccessLevel(): int
    {
        return self::USER_ROLES[$this->role];
    }

    public function __toString(): string
    {
        return $this->role;
    }

    public static function fromAccessLeven(int $accessLevel): string
    {
        return array_search($accessLevel, self::USER_ROLES);
    }
}
