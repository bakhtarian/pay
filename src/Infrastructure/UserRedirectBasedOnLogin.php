<?php

declare(strict_types=1);

namespace Pay\Infrastructure;

use Pay\Domain\User\User;
use Zend\Diactoros\Response\RedirectResponse;

trait UserRedirectBasedOnLogin
{
    public function isLoggedIn()
    {
        if ($_SESSION['user'] instanceof User) {
            return new RedirectResponse('/overview');
        }
    }

    public function mayViewPage()
    {
        if (isset($_SESSION['user']) === false || !$_SESSION['user' ] instanceof User) {
            return  new RedirectResponse('/login');
        }
    }
}
