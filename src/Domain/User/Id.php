<?php

declare(strict_types=1);

namespace Pay\Domain\User;

use Assert\Assertion;

final class Id
{
    private string $id;

    public function __construct(string $id)
    {
        Assertion::uuid($id);

        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
