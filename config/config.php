<?php

use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Pay\Domain\User\UserRepository;
use Pay\Infrastructure\Repository\DataBase;
use Pay\Infrastructure\Repository\MysqlUserRepository;
use Pay\Infrastructure\Session;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequestFactory;

return [
    'request' => function () {
        return ServerRequestFactory::fromGlobals(
            $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
        );
    },
    'response' => new Response(),
    'emitter' => new SapiEmitter(),
    Environment::class => function () {
        $loader = new FilesystemLoader(dirname(__DIR__).'/html/views');

        return new Environment($loader);
    },
    UserRepository::class => function () {
        $host = 'mysql';
        $database = 'pay';
        $user = 'pay';
        $password = 'pay';

        $database = new DataBase(
            $host, $user, $password, $database
        );

        return new MysqlUserRepository($database);
    },
    Session::class => function () {
        return new Session();
    }
];
