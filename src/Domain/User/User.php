<?php

declare(strict_types=1);

namespace Pay\Domain\User;

use Pay\Domain\Exception\NotAllowedToChangeUserException;
use Ramsey\Uuid\Uuid;

final class User
{
    private Id $id;
    private Email $email;
    private Password $password;
    private Role $role;
    private bool $active;
    private \DateTimeImmutable $createdAt;
    private ?\DateTimeImmutable $modifiedAt;
    private ?\DateTimeImmutable $terminatedAt;

    public function getId(): Id
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modifiedAt;
    }

    public function getTerminatedAt(): ?\DateTimeImmutable
    {
        return $this->terminatedAt;
    }

    public static function create(Email $email, Password $password)
    {
        $user = new self();
        $user->id = new Id((string) Uuid::uuid4());
        $user->email = $email;
        $user->password = $password->hasPassword();
        $user->role = new Role(Role::BASE_USER_NAME);
        $user->active = true;
        $user->createdAt = new \DateTimeImmutable();

        return $user;
    }

    public function changeRole(string $role, User $user): self
    {
        $this->mutabilityAllowed($user);

        $this->role = new Role($role);
        $this->modifiedAt = new \DateTimeImmutable();

        return $this;
    }

    public function changePassword(string $password, User $user): self
    {
        $this->mutabilityAllowed($user);

        $this->password = (new Password($password))->hasPassword();
        $this->modifiedAt = new \DateTimeImmutable();

        return new self();
    }

    public function changeEmail(string $email, User $user): self
    {
        $this->mutabilityAllowed($user);

        $this->email = new Email($email);
        $this->modifiedAt = new \DateTimeImmutable();

        return $this;
    }

    public function terminate(User $user): self
    {
        $this->mutabilityAllowed($user);

        $this->terminatedAt = new \DateTimeImmutable();
        $this->active = false;
        $this->role = new Role(Role::TERMINATED_USER_NAME);
        $obfuscatedMail = substr(hash('sha512', (string) $this->email), -15);
        $this->email = new Email($obfuscatedMail.'@obfuscate.com');
        $this->modifiedAt = new \DateTimeImmutable();

        return $this;
    }

    public function mutabilityAllowed(User $user): void
    {
        if ($user->role->getAccessLevel() < $this->role->getAccessLevel()) {
            throw NotAllowedToChangeUserException::forUser($this->id, $user->id);
        }
    }

    public static function fromArray(array $userData): self
    {
        $user = new self();

        $user->id = new Id($userData['id']);
        $user->email = new Email($userData['email']);
        $user->password = new Password($userData['password']);
        $user->role = new Role(Role::fromAccessLeven($userData['role']));
        $user->active = (bool) $userData['active'];
        $user->createdAt = new \DateTimeImmutable($userData['created_at']);
        $user->modifiedAt = $userData['modified_at'] ? new \DateTimeImmutable($userData['modified_at']) : nulL;
        $user->terminatedAt = $userData['terminated_at'] ? new \DateTimeImmutable($userData['terminated_at']) : nulL;

        return $user;
    }
}
