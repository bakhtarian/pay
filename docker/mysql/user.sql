CREATE DATABASE IF NOT EXISTS pay;

USE pay;

CREATE TABLE IF NOT EXISTS users (
     id varchar(36) NOT NULL,
     email varchar(255) NOT NULL UNIQUE ,
     password varchar (255) NOT NULL,
     role TINYINT DEFAULT 0,
     active bool DEFAULT FALSE,
     created_at datetime NOT NULL,
     modified_at datetime DEFAULT NULL,
     terminated_at date DEFAULT NULL,
     PRIMARY KEY (id)
);

INSERT INTO users (
    id,
    email,
    password,
    role,
    active,
    created_at,
    modified_at,
    terminated_at
) VALUES
 (
    'f6dc20e5-6ceb-4552-b5a0-bac6d3e76948',
    'base@pay.nl',
    '$2y$10$ITHlUonNa69jXlRTzc15fuZ315vKKkUDORXuwTIzMh3BpOcYT.COi',
    1,
    true,
    '2020-04-13 10:10:10',
    null,
    null
),
(
    '0760fa3e-b45d-4a40-b512-4e36f855be1e',
    'admin@pay.nl',
    '$2y$10$U93Zg4731d2PZJZATf5JROHgWmMbv7JkeH4q2dzGm9XVCMjbi5gke',
    2,
    true,
    '2020-04-13 10:10:11',
    null,
    null
),
(
    '0bff6dbd-1e1b-4164-9181-a72db1f92611',
    'super@pay.nl',
    '$2y$10$C4jOHOoeEKW7z8sqfpqJDe6aHa9LZlO5itMiouRLq1qUuG33LvsyO',
    3,
    true,
    '2020-04-13 10:10:12',
    null,
    null
);
